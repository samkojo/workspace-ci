import { add } from './add.js'

describe('add function', () => {
  it('adding two numbers', () => {
    const result = add(10, 5)

    expect(result).toBe(15)
  })
})
