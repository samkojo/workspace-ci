if [ -z $CI_MERGE_REQUEST_DIFF_BASE_SHA ]; then
  yarn commitlint --from=HEAD~1
else
  yarn commitlint --from=$CI_MERGE_REQUEST_DIFF_BASE_SHA
fi;
